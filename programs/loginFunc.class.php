<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';
require_once dirname(__FILE__) . '/functions.php';


bab_functionality::includeFile('PortalAuthentication');


class Func_PortalAuthentication_AuthClassic extends Func_PortalAuthentication
{

    public function getDescription()
    {
        return AuthClassic_translate("Classic authentication");
    }

    public function getAuthForm()
    {
        $W = bab_Widgets();
        $addon = bab_getAddonInfosInstance('AuthClassic');
        
        $form = $W->Form()->addClass('col-md-3 col-md-offset-4 margClass')/*->setName('AuthClassic')*/;
        $controller = AuthClassic_Controller();

        $submit = $W->SubmitButton()->validate()->addClass('col-md-10 col-md-offset-1 margClass')
            ->setAction($controller->Login()->login())
            ->setLabel(AuthClassic_translate('Log in'));
        $submit->setAction();

        $layout = $W->VBoxItems()->setVerticalSpacing(1, 'em');
        
        $registry = bab_getRegistryInstance();
        $registry->changeDirectory('/AuthClassic/');
        
        $wName = $registry->getValue('NicknameWidget', 'LineEdit');
        $input = $W->$wName(); // EmailLineEdit
        
        $input->setMandatory(true, AuthClassic_translate('The login is mandatory.'));
        $input->addClass('form-control');
        
        $layout->addItem(
            $W->LabelledWidget(
                $registry->getValue('NicknameLabel', AuthClassic_translate('Login')),
                $input,
                'nickname'
            )->setSizePolicy('col-md-12')
        );
        
        $layout->addItem(
            $W->LabelledWidget(
                AuthClassic_translate('Password'),
                $W->LineEdit()->setMandatory(true, AuthClassic_translate('The password is mandatory.'))->obfuscate()->addClass('form-control'),
                'password'
            )->setSizePolicy('col-md-12')
        );
        
        $layout->addItem($submit);
        

        $form->setLayout($layout);

        if (bab_isAjaxRequest()) {
            $action = $controller->Login()->login();
            $submit->setAjaxAction($action);
        } else {
            $submit->setName('');
            $form->setHiddenValue('tg', 'login');
            $form->setHiddenValue('referer', AuthClassic_getUrl());
            $form->setHiddenValue('login', 'login'); // This is important for the bab_storeHttpContext()
            $form->setHiddenValue('sAuthType', 'Classic');
        }

        return $W->VBoxItems(
            $W->Html(bab_printOvmlTemplate($addon->getRelativePath().'login.html')),
            $form
        );
    }

    public function authenticate()
    {
        $W = bab_Widgets();
        echo $this->getAuthForm()->display($W->HtmlCanvas());
        exit;
    }

    public function login()
    {
        if ($this->isLogged())
        {
            return true;
        }

        require_once $GLOBALS['babInstallPath'] . 'utilit/loginIncl.php';


        $login		= bab_pp('nickname', null);
        $password	= bab_pp('password', null);

        if ($login !== null && $password !== null)
        {
            $idUser = $this->authenticateUser($login, $password);

            if($idUser && $this->userCanLogin($idUser))
            {
                $this->setUserSession($idUser, 365 * 86400);
                return true;
            }


            if (bab_isAjaxRequest()) {
                header('HTTP/1.0 401 Unauthorized');
                foreach($this->errorMessages as $message)
                {
                    echo $message." \n";
                }
                exit;
            }
        }
        $controller = AuthClassic_Controller();
        if (bab_isAjaxRequest()) {
            $W = bab_Widgets();
            header('WWW-Authenticate: AuthClassic realm="' . $GLOBALS['babSiteName'] . '"');
            header('HTTP/1.0 401 Unauthorized');
            echo $controller->Login(false)->displayForm($this->loginMessage, implode("\n", $this->errorMessages))->display($W->HtmlCanvas());
            exit;
        } else {
            if (isset($_SERVER['HTTP_REFERER'])) {
                $controller->Login()->displayForm($this->loginMessage, implode("\n", $this->errorMessages), $_SERVER['HTTP_REFERER'])->location();
            }
            
            $controller->Login()->displayForm($this->loginMessage, implode("\n", $this->errorMessages))->location();
        }
        return false;
    }

    public function logout()
    {
        $AuthObject = bab_functionality::get('PortalAuthentication/AuthOvidentia');
        return $AuthObject->logout();
    }
}
